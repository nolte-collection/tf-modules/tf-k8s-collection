variable "env" {

}

locals {
  ingress_domains = {
    docker    = "172-17-0-1.sslip.io"
    kind    = "192-168-178-51.sslip.io"
    vagrant = "172-17-177-11.sslip.io"
    cubox   = "192-168-178-88.sslip.io"
  }
}

locals {
  ingress_domain = local.ingress_domains[var.env]
}


variable "auth_base_real" {
  default = "devops"
}
output "auth_base_real" {
  value = var.auth_base_real
}
output "ingress_domain" {
  value = local.ingress_domain
}
locals {
  DOCKER_REGISTRY = format("harbor.%s", local.ingress_domain)
  AUTH_SERVICE    = format("keycloak.%s", local.ingress_domain)
  GIT_SERVICE     = format("gitea.%s", local.ingress_domain)
}

output "authservice_host" {
  value = local.AUTH_SERVICE
}
output "authservice_url" {
  value = format("https://%s", local.AUTH_SERVICE)
}

output "public_oidc_endpoint" {
  # https://keycloak.192-168-178-51.sslip.io/auth/realms/devops
  value = format("https://keycloak.%s/auth/realms/%s", local.ingress_domain, var.auth_base_real)
}


output "public_gitea_endpoint" {
  value = format("https://gitea.%s", local.ingress_domain)
}

output "git_host" {
  value = local.GIT_SERVICE
}

output "registry_host" {
  value = local.DOCKER_REGISTRY
}
output "registry_url" {
  value = format("https://%s", local.DOCKER_REGISTRY)
}


output "repository_project_name" {
  value = "devops"
}
