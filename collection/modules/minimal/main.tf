resource "kubernetes_namespace" "operators" {
  metadata {
    name = "operators"
  }
}
resource "kubernetes_namespace" "certmanager" {
  metadata {
    name = "cert-manager"
  }
}


variable "ingress_domain" {

}

module "prometheus_crds" {
  source                     = "../../../modules/k8s-monitoring/modules/crds"
  enable_crd_servicemonitors = true
  enable_crd_prometheusrules = true
  enable_crd_alertmanagers   = true
  enable_crd_podmonitors     = true
  enable_crd_prometheuses    = true
  enable_crd_thanosrulers    = true
}

module "forecastle_crds" {
  source                     = "../../../modules/k8s-forecastle/modules/crds"
}

module "certmanager" {
  depends_list      = [module.prometheus_crds.depend_on]
  source                     = "../../../modules/k8s-certmanager/modules/install"
  release_namespace = kubernetes_namespace.certmanager.metadata[0].name
}

module "certmanager_app" {
  depends_list      = [module.forecastle_crds.depend_on,module.certmanager.depend_on]
  source = "../../../modules/k8s-forecastle/modules/application"
  namespace = kubernetes_namespace.certmanager.metadata[0].name
  name = "certmanager-operator"
  readable_name = "CertManager"
  url = "https://cert-manager.io"
  icon = "https://raw.githubusercontent.com/jetstack/cert-manager/master/logo/logo.png"
}

# give Certsmanger Time to Work
resource "time_sleep" "wait_seconds" {
  depends_on = [module.certmanager.depend_on]

  create_duration = "15s"
}

module "selfsigned_ca" {
  depends_list = [
    time_sleep.wait_seconds,
    module.certmanager.depend_on
  ]
  source = "../../../modules/k8s-certmanager/modules/selfsigned_ca"
}


module "postgres" {
  depends_list      = [
    module.prometheus_crds.depend_on
  ]
  source                     = "../../../modules/k8s-zalando-postgres/modules/install"
}

module "postgres_app" {
  depends_list      = [
    module.forecastle_crds.depend_on,
    module.postgres.depend_on,
  ]
  source = "../../../modules/k8s-forecastle/modules/application"
  namespace = kubernetes_namespace.operators.metadata[0].name #module.postgres.release_namespace
  name = "postgres-operator"
  readable_name = "Zalando Postgres Operator"
  url = "https://github.com/zalando/postgres-operator"
  icon = "https://raw.githubusercontent.com/zalando/postgres-operator/master/docs/diagrams/logo.png"
}


resource "time_sleep" "wait_seconds_cluster_issuer" {
  depends_on = [module.selfsigned_ca.depend_on]

  create_duration = "15s"
}

module "minio" {
  depends_list      = [
    time_sleep.wait_seconds_cluster_issuer,
    module.prometheus_crds.depend_on
  ]

  source            = "../../../modules/k8s-minio/modules/install"
  ingress_domain = var.ingress_domain
}



module "forecastle_app" {
  depends_list      = [
    time_sleep.wait_seconds_cluster_issuer,
    module.forecastle_crds.depend_on
  ]
  source                     = "../../../modules/k8s-forecastle/modules/install"
  ingress_domain = var.ingress_domain
  skip_crds = true
}
module "reloader" {
  source                     = "../../../modules/k8s-reloader/modules/install"
  namespace = kubernetes_namespace.operators.metadata[0].name
}
module "reloader_app" {
  depends_list      = [
    module.forecastle_crds.depend_on,
    module.postgres.depend_on,
  ]
  source = "../../../modules/k8s-forecastle/modules/application"
  namespace = kubernetes_namespace.operators.metadata[0].name #module.postgres.release_namespace
  name = "reloader"
  readable_name = "Reloader"
  url = "https://github.com/stakater/Reloader"
  icon="https://raw.githubusercontent.com/stakater/Reloader/master/assets/web/reloader-round-100px.png"
}
module "proxy_injector" {
  source                     = "../../../modules/k8s-proxy-injector/modules/install"
  namespace = kubernetes_namespace.operators.metadata[0].name
}
module "proxy_injector_app" {
  depends_list      = [
    module.forecastle_crds.depend_on,
    module.postgres.depend_on,
  ]
  source = "../../../modules/k8s-forecastle/modules/application"
  namespace = kubernetes_namespace.operators.metadata[0].name #module.postgres.release_namespace
  name = "proxy-injector"
  readable_name = "Proxy Injector"
  url = "https://github.com/stakater/ProxyInjector"
  icon = "https://raw.githubusercontent.com/stakater/ProxyInjector/master/assets/web/proxyinjector-round-100px.png"
}

module "prometheus_operator" {
  depends_list      = [
    module.prometheus_crds.depend_on
  ]
  source                     = "../../../modules/k8s-monitoring/modules/operator"
  operator_namespace = kubernetes_namespace.operators.metadata[0].name
}
resource "time_sleep" "wait_seconds_prometheus_operator" {
  depends_on = [module.prometheus_operator.depend_on]

  create_duration = "15s"
}

module "monitoring_prometheus" {
  depends_list   = [
    time_sleep.wait_seconds_cluster_issuer,
    time_sleep.wait_seconds_prometheus_operator
  ]
  source         = "../../../modules/k8s-monitoring/modules/prometheus"
  ingress_domain = var.ingress_domain
}

module "prometheus_operator_app" {
  depends_list      = [
    module.forecastle_crds.depend_on,
    module.certmanager.depend_on
  ]

  source = "../../../modules/k8s-forecastle/modules/application"
  namespace = module.monitoring_prometheus.namespace
  name = "prometheus-operator"
  readable_name = "Prometheus Operator"
  url = "https://github.com/coreos/prometheus-operator"
  icon = "https://www.vectorlogo.zone/logos/prometheusio/prometheusio-ar21.svg"
}


module "keycloak" {
  depends_list   = [
    time_sleep.wait_seconds_cluster_issuer,
    module.postgres.depend_on,
    module.certmanager.depend_on,
    module.prometheus_crds.depend_on,
  ]
  source         = "../../../modules/k8s-keycloak/modules/install"
  ingress_domain = var.ingress_domain
}

module "harbor" {
  depends_list   = [
    time_sleep.wait_seconds_cluster_issuer,
    module.certmanager.depend_on,
    module.prometheus_crds.depend_on,
  ]
  source         = "../../../modules/k8s-harbor/modules/install"
  ingress_domain = var.ingress_domain
}
