output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    module.certmanager.depend_on,
    module.keycloak.depend_on,
    module.harbor.depend_on,
  ]
}
