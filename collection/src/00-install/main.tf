module "meta_data" {
  source         = "../../modules/metadata"
  env = "docker"
}

module "minimal_cluster_config" {
  source            = "../../modules/minimal"
  ingress_domain    = module.meta_data.ingress_domain
}
