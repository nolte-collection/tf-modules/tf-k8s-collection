module "meta_data" {
  source         = "../../modules/metadata"
  env = "docker"
}


module "harbor" {
  source         = "../../../modules/k8s-harbor/modules/configure_base"
  name = "base-projects"
}


module "gitea" {
  source         = "../../../modules/k8s-gitea/modules/install"
  ingress_domain = module.meta_data.ingress_domain
  oidc_auto_discover_url = format("%s/.well-known/openid-configuration", module.meta_data.public_oidc_endpoint)
}

output "depend_on" {
  value = [
    module.harbor.depend_on,
    module.gitea.depend_on
  ]
}
