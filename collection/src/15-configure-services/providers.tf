terraform {
}


data "kubernetes_secret" "keycloak_admin_token" {
  metadata {
    name      = "keycloak-admin-auth"
    namespace = "keycloak"
  }
}
data "kubernetes_ingress" "keycloak" {
  metadata {
    name = "keycloak"
    namespace = "keycloak"
  }
}

provider "keycloak" {
  client_id                = "admin-cli"
  username                 = "keycloak"
  password                 = data.kubernetes_secret.keycloak_admin_token.data.password
  # url                      = "http://localhost:8086"
  url = "https://${data.kubernetes_ingress.keycloak.spec.0.rule.0.host}"
  tls_insecure_skip_verify = true
}




data "kubernetes_secret" "harbor_admin" {

  metadata {
    name      = "harbor-harbor-core"
    namespace = "harbor"
  }
}

data "kubernetes_ingress" "harbor" {
  metadata {
    name = "harbor-harbor-ingress"
    namespace = "harbor"
  }
}

provider "harbor" {
  host     = data.kubernetes_ingress.harbor.spec.0.rule.0.host
  schema   = "https"
  insecure = true
  basepath = "/api/v2.0"
  username = "admin"
  password = data.kubernetes_secret.harbor_admin.data["HARBOR_ADMIN_PASSWORD"]
}
