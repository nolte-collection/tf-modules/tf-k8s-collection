module github.com/nolte/tf-k8s-collection

go 1.14

require (
	github.com/gruntwork-io/terratest v0.28.12
	github.com/magefile/mage v1.10.0
)
