//+build mage

package main

import (
	"context"
	"os"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

var Aliases = map[string]interface{}{
	"all": Build,
}

func Install() error {
	os.Chdir("collection/src/00-install")
	defer os.Chdir("../../../")
	sh.Run("terraform", "init")
	return sh.Run(
		"terraform", "apply", "-auto-approve", "-parallelism=5")
}

func Configure() error {
	os.Chdir("collection/src/10-configure-auth")
	defer os.Chdir("../../../")
	sh.Run("terraform", "init")
	return sh.Run(
		"terraform", "apply", "-auto-approve")
}
func ConfigureServices() error {
	os.Chdir("collection/src/15-configure-services")
	defer os.Chdir("../../../")
	sh.Run("terraform", "init")
	return sh.Run(
		"terraform", "apply", "-auto-approve")
}
func Build(ctx context.Context) {
	mg.CtxDeps(ctx, Install)
	mg.CtxDeps(ctx, Configure)
	mg.CtxDeps(ctx, ConfigureServices)
}
