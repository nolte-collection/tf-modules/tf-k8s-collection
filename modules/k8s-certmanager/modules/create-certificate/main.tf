variable "name" {
}
variable "namespace" {
}

variable "host" {
}
variable "svc" {
}
resource "kubectl_manifest" "certmanager_cert" {
  depends_on = [var.depends_list]
  yaml_body  = <<YAML
---
apiVersion: cert-manager.io/v1beta1
kind: Certificate
metadata:
  name: "${var.name}"
  namespace: "${var.namespace}"
spec:
  secretName: "${var.name}"
  dnsNames:
  - ${var.host}
  - ${var.svc}.${var.namespace}.svc
  issuerRef:
    name: "cluster-issuer"
    kind: ClusterIssuer
    YAML
}

variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [kubectl_manifest.certmanager_cert]
}
