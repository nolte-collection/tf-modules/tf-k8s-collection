module "crds" {
  source = "../../modules/crds"
}

module "install" {
  depends_list      = [module.crds.depend_on]
  source = "../../modules/install"
  ingress_domain = "172-17-0-1.sslip.io" #"notexists.local"
  skip_crds = true
}


module "test_app" {
  depends_list      = [module.crds.depend_on]
  source = "../../modules/application"
  namespace = "forecastle"
  name = "test-app"
  readable_name = "Readable ProjectName"
}
