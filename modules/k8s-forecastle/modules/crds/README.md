# Module install for Helm Chart cert-manager

``` hcl
module "prometheus_crds" {
  source                     = "git::https://github.com/nolte/tf-k8s-monitoring.git//modules/crds"
  enable_crd_servicemonitors = true
  enable_crd_prometheusrules = true
  enable_crd_alertmanagers   = true
  enable_crd_podmonitors     = true
  enable_crd_prometheuses    = true
  enable_crd_thanosrulers    = true
}
```
