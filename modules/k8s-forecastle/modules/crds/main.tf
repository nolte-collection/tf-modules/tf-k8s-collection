

locals {
  forecastle_app_crd_base_url = "https://raw.githubusercontent.com/stakater/Forecastle/${var.forecastle_app_operator_version}/deployments/kubernetes/chart/forecastle/crds"
}

data "http" "forecastle_app" {
  url = "${local.forecastle_app_crd_base_url}/forecastleApp.yaml"
}

resource "kubectl_manifest" "forecastle_app" {
  yaml_body = data.http.forecastle_app.body
}

