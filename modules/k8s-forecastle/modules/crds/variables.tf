variable "forecastle_app_operator_version" {
  default = "v1.0.54"
}

variable "depends_list" {
  default = []
}
