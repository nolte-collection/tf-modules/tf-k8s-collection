# https://github.com/stakater/Forecastle/blob/master/deployments/kubernetes/chart/forecastle/values.yaml
forecastle:
    createCustomResource: false
    config:
        crdEnabled: true
        namespaceSelector:
            any: true
    ingress:
        annotations: {}
