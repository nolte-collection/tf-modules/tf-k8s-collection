
variable "namespace" {
  default = "forecastle"
}

variable "ingress_domain" {

}

variable "skip_crds" {
  default = false
}

module "static_ingress_data" {
  source = "../../../k8s-static-data/ingress"
}


locals {
  ingress_forecastle_annotations =  {
    "forecastle.stakater.com/appName":"Forecastle",
    "forecastle.stakater.com/group": "utils",
    "forecastle.stakater.com/icon": "https://raw.githubusercontent.com/stakater/Forecastle/master/assets/web/forecastle-round-100px.png",
  }
}

locals {
  ingress_annotations  = {
    forecastle = {
      ingress = {
        annotations =  merge( module.static_ingress_data.default_annotations,local.ingress_forecastle_annotations)
      }
    }
  }
}

resource "helm_release" "release" {
  name       = "forecastle"
  repository = "https://stakater.github.io/stakater-charts"
  chart      = "forecastle"
  namespace  = var.namespace
  create_namespace = true
  skip_crds = var.skip_crds
  values = [
    "${templatefile("${path.module}/files/values.yml.tpl", {})}",
    yamlencode(local.ingress_annotations)
  ]
  set {
    name  = "forecastle.namespace"
    value =  var.namespace
  }
  set {
    name  = "forecastle.ingress.enabled"
    value = "true"
  }
  #set {
  #  name  = "forecastle.ingress.annotations"
  #  value =  "{ ${yamlencode(var.ingress_annotations)} }"
  #}
  set {
    name  = "forecastle.ingress.hosts[0].host"
    value =  "forecastle.${var.ingress_domain}"
  }
  set {
    name  = "forecastle.ingress.hosts[0].paths[0]"
    value =  "/"
  }
   set {
    name  = "forecastle.ingress.tls[0].secretName"
    value =  "forecastle-cert"
  }
  set {
    name  = "forecastle.ingress.tls[0].hosts[0]"
    value =  "forecastle.${var.ingress_domain}"
  }
}
