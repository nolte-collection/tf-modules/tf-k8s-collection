resource "kubernetes_namespace" "git_namespace" {
  depends_on = [var.depends_list]
  metadata {
    name = "gitea"
  }
}

variable "realm_name" {
  default = "devops"
}
variable "oidc_client_key" {
  default = "gitea"

}

module "oidc_client" {
  source                     = "../../../k8s-keycloak/modules/oidc_client"
  keycloak_service_client_id = var.oidc_client_key
  keycloak_service_name      = "gitea"
  realm_name                 = var.realm_name
  service_redirect_uri       = ["https://gitea.${var.ingress_domain}/c/oidc/callback"]
}
module "static_ingress_data" {
  source = "../../../k8s-static-data/ingress"
}
locals {
  ingress_minio_annotations =  {
     "forecastle.stakater.com/appName":"Gitea",
     "forecastle.stakater.com/icon": "https://raw.githubusercontent.com/go-gitea/gitea/master/assets/logo.svg",
     "forecastle.stakater.com/group": "development",
     "ingress.kubernetes.io/proxy-body-size": "0",
     "nginx.ingresskubernetes.io/proxy-body-size": "0",
  }
}
locals {
  ingress_annotations  = {
    ingress = {
         ingress_annotations = merge( module.static_ingress_data.default_annotations,local.ingress_minio_annotations)
    }
  }
}

resource "helm_release" "gitea" {
  depends_on = [module.oidc_client.depend_on]
  name       = "gitea"
  repository = "https://nolte.github.io/helm-charts/"
  chart      = "gitea"
  version    = "1.10.1"
  namespace  = kubernetes_namespace.git_namespace.metadata[0].name
  values = [
    "${templatefile("${path.module}/files/gitea-chart-values.yml", {
      host                   = "gitea.${var.ingress_domain}",
      OIDC_CLIENT_KEY        = var.oidc_client_key
      OIDC_CLIENT_SECRET     = module.oidc_client.client_secret
      OIDC_AUTO_DISCOVER_URL = var.oidc_auto_discover_url
    })}",
    yamlencode(local.ingress_annotations),
  ]
}

data "kubernetes_secret" "gitea_admin_token" {
  depends_on = [helm_release.gitea, var.depends_list]
  metadata {
    name      = "gitea-admin-token"
    namespace = kubernetes_namespace.git_namespace.metadata[0].name
  }
}
output "gitea_admin_token" {
  value = data.kubernetes_secret.gitea_admin_token.data["token"]
}

variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    helm_release.gitea.id,
    data.kubernetes_secret.gitea_admin_token,
    module.oidc_client.depend_on,
    kubernetes_namespace.git_namespace,
  ]
}
