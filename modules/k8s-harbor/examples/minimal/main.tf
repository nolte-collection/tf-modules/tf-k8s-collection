
resource "kubernetes_namespace" "operators" {
  metadata {
    name = "operators"
  }
}

module "prometheus_crds" {
  source                     = "../../../k8s-monitoring/modules/crds"
  enable_crd_servicemonitors = true
  enable_crd_prometheusrules = true
  enable_crd_alertmanagers   = true
  enable_crd_podmonitors     = true
  enable_crd_prometheuses    = true
  enable_crd_thanosrulers    = true
}


module "certmanager" {
  depends_list      = [module.prometheus_crds.depend_on]
  source                     = "../../../k8s-certmanager/modules/install"
  release_namespace = kubernetes_namespace.operators.metadata[0].name
}

# give Certsmanger Time to Work
resource "time_sleep" "wait_seconds" {
  depends_on = [module.certmanager.depend_on]

  create_duration = "15s"
}

module "selfsigned_ca" {
  depends_list = [
    time_sleep.wait_seconds,
    module.certmanager.depend_on
  ]
  source = "../../../k8s-certmanager/modules/selfsigned_ca"
}


module "install" {
  depends_list      = [
    module.selfsigned_ca.depend_on
  ]
  source = "../../modules/install"
  ingress_domain = "172-17-0-1.sslip.io"
}

