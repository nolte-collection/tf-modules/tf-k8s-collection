
#module "meta_data" {
#  source = "../../common_metadata"
#}


variable "realm_name" {
  default = "devops"
}

data "keycloak_realm" "realm" {
  realm = var.realm_name
}
variable "harbor_url" {
  default = "http://localhost:8080"
}

locals {
  harbor_redirect_uri = format("%s/c/oidc/callback", var.harbor_url)
}
variable "keycloak_harbor_client_id" {
  default = "harbor-client"
}


resource "keycloak_openid_client" "openid_client" {
  depends_on = [var.depends_list]

  realm_id  = "${data.keycloak_realm.realm.id}"
  client_id = var.keycloak_harbor_client_id

  name                  = "harbor client"
  enabled               = true
  standard_flow_enabled = true
  access_type           = "CONFIDENTIAL"
  valid_redirect_uris = [
    local.harbor_redirect_uri
  ]
}
variable "oidc_endpoint" {

}

resource "harbor_config_auth" "oidc" {
  auth_mode          = "oidc_auth"
  oidc_name          = "keycloak"
  oidc_endpoint      = var.oidc_endpoint
  oidc_client_id     = keycloak_openid_client.openid_client.client_id
  oidc_client_secret = keycloak_openid_client.openid_client.client_secret
  oidc_scope         = "openid,email"
  oidc_verify_cert   = true
}

output "client_secret" {
  value = keycloak_openid_client.openid_client.client_secret
}


variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    keycloak_openid_client.openid_client,
  ]
}

