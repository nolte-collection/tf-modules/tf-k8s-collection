
#module "meta_data" {
#  source = "../../common_metadata"
#}

variable "name" {

}


resource "harbor_project" "main" {
  depends_on             = [var.depends_list]
  name                   = var.name
  public                 = true # (Optional) Default value is false
  vulnerability_scanning = true # (Optional) Default vale is true. Automatically scan images on push
}

resource "harbor_registry" "docker" {
  name        = "dockerhub"
  url         = "https://hub.docker.com"
  type        = "docker-hub"
  description = "Docker Hub"
}
resource "harbor_registry" "codecentric" {
  name        = "codecentric"
  url         = "https://codecentric.github.io/helm-charts"
  type        = "helm-hub"
  description = "Helm Chart codecentric Repo"
}
resource "harbor_registry" "bitnami" {
  name        = "bitnami"
  url         = "https://charts.bitnami.com/bitnami"
  type        = "helm-hub"
  description = "Helm Chart bitnami Repo"
}

resource "harbor_registry" "helmhub" {
  name        = "helmhub"
  url         = "https://hub.helm.sh"
  type        = "helm-hub"
  description = "Helm Chart Repo"
}

resource "harbor_registry" "jetstack" {
  name        = "jetstack"
  url         = "https://charts.jetstack.io"
  type        = "helm-hub"
  description = "jetstack Registry"
  insecure    = false
}

resource "harbor_project" "mirror" {
  depends_on             = [var.depends_list]
  name                   = "mirror"
  public                 = true # (Optional) Default value is false
  vulnerability_scanning = true # (Optional) Default vale is true. Automatically scan images on push
}

resource "harbor_replication" "helm_certmanager" {
  name                        = "certmanager-operator"
  description                 = "certmanager Operator Replica"
  source_registry_id          = harbor_registry.jetstack.id
  source_registry_filter_name = "jetstack/cert-manager"
  source_registry_filter_tag  = "**"
  destination_namespace       = harbor_project.mirror.name
  enabled                     = true
  trigger_mode                = "scheduled"
  trigger_cron                = "* * 5 * * *"
}

resource "harbor_replication" "helm_prometheus_operator" {
  name                        = "prometheus-operator"
  description                 = "Prometheus Operator Replica"
  source_registry_id          = harbor_registry.helmhub.id
  source_registry_filter_name = "stable/prometheus-operator"
  source_registry_filter_tag  = "**"
  destination_namespace       = harbor_project.mirror.name
  enabled                     = true
  trigger_mode                = "scheduled"
  trigger_cron                = "* * 4 * * *"
}

resource "harbor_replication" "helm_keycloak" {
  name                        = "keycloak"
  description                 = "keycloak Replica"
  source_registry_id          = harbor_registry.codecentric.id
  source_registry_filter_name = "codecentric/keycloak"
  source_registry_filter_tag  = "**"
  destination_namespace       = harbor_project.mirror.name
  enabled                     = true
  trigger_mode                = "scheduled"
  trigger_cron                = "* * 4 * * *"
}
resource "harbor_replication" "helm_karma" {
  name                        = "karma"
  description                 = "karma Replica"
  source_registry_id          = harbor_registry.helmhub.id
  source_registry_filter_name = "stable/karma"
  source_registry_filter_tag  = "**"
  destination_namespace       = harbor_project.mirror.name
  enabled                     = true
  trigger_mode                = "scheduled"
  trigger_cron                = "* * 4 * * *"
}
resource "harbor_replication" "helm_home_assistant" {
  name                        = "home-assistant"
  description                 = "home-assistant Replica"
  source_registry_id          = harbor_registry.helmhub.id
  source_registry_filter_name = "stable/home-assistant"
  source_registry_filter_tag  = "**"
  destination_namespace       = harbor_project.mirror.name
  enabled                     = true
  trigger_mode                = "scheduled"
  trigger_cron                = "* * 4 * * *"
}

















output "repository_project_name" {
  value = harbor_project.main.name
}
output "repository_project_id" {
  value = harbor_project.main.id
}

variable "depends_list" {
  default = []

}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    harbor_project.main,
  ]
}

