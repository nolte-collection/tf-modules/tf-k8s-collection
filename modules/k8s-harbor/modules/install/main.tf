
resource "kubernetes_namespace" "harbor" {
  depends_on = [var.depends_list]
  metadata {
    name = "harbor"
  }
}


variable "ingress_domain" {
}

module "static_ingress_data" {
  source = "../../../k8s-static-data/ingress"
}
locals {
  ingress_minio_annotations =  {
     "forecastle.stakater.com/icon": "https://raw.githubusercontent.com/goharbor/website/master/static/img/logos/harbor-icon-color.png",
     "forecastle.stakater.com/group": "development",
     "ingress.kubernetes.io/proxy-body-size": "0",
     "nginx.ingresskubernetes.io/proxy-body-size": "0",
  }
}
locals {
  ingress_annotations  = {
    expose = {
      ingress = {
           annotations = merge( module.static_ingress_data.default_annotations,local.ingress_minio_annotations)
      }
    }
  }
}


#module "harbor_cert" {
#  source = "../../../k8s-certmanager/modules/create-certificate"
#  name = "harbor-cert"
#  namespace = kubernetes_namespace.harbor.metadata[0].name
#  host = format("harbor.%s", var.ingress_domain)
#  svc = "harbor-harbor-core"
#}
#module "notary_cert" {
#  source = "../../../k8s-certmanager/modules/create-certificate"
#  name = "notary-cert"
#  namespace = kubernetes_namespace.harbor.metadata[0].name
#  host = format("notary.%s", var.ingress_domain)
#  svc = "harbor-harbor-notary-server"
#}


resource "random_password" "harbor_secret_key" {
  length           = 16
  special          = true
  override_special = "-"
}

resource "helm_release" "harbor" {
  name       = "harbor"
  repository = "https://helm.goharbor.io"
  chart      = "harbor"
  namespace  = kubernetes_namespace.harbor.metadata[0].name
  timeout    = 600
  values = [
    "${templatefile("${path.module}/files/values.yml", {
      CORE_INGRESS          = format("harbor.%s", var.ingress_domain),
      NOTARY_INGRESS        = format("notary.%s", var.ingress_domain),
    })}",
    yamlencode(local.ingress_annotations)
  ]
  set_sensitive {
    name = "harborAdminPassword"
    value = "Harbor12345"
  }
  # The secret key used for encryption. Must be a string of 16 chars.
  set_sensitive {
    name = "secretKey"
    value = random_password.harbor_secret_key.result
  }
  #version = "v0.9.0-rc.4"
}

variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    helm_release.harbor,
  ]
}

