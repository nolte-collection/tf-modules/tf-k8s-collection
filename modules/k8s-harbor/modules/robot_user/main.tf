
#variable "project_name" {
#
#}
variable "project_id" {

}


variable "robot_name" {
  default = "foffa"
}
variable "robot_description" {
  default = "Robot account used to push images to harbor"
}

#data "harbor_project" "main" {
#  name = var.project_name
#}

resource "harbor_robot_account" "account" {
  name        = var.robot_name
  description = var.robot_description
  project_id  = var.project_id #data.harbor_project.main.project_id
  actions     = ["docker_read", "docker_write", "helm_read", "helm_write"]
}

output "token" {
  value = harbor_robot_account.account.token
}
output "name" {
  value = format("robot$%s", harbor_robot_account.account.name)
}

variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    harbor_robot_account.account,
  ]
}

