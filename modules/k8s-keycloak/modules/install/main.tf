
resource "kubernetes_namespace" "keycloak" {
  depends_on = [var.depends_list]
  metadata {
    name = "keycloak"
  }
}

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource "kubernetes_secret" "keycloak_admin_auth" {
  metadata {
    name      = "keycloak-admin-auth"
    namespace = kubernetes_namespace.keycloak.metadata[0].name
  }

  data = {
    password = random_password.password.result
  }
}
variable "ingress_domain" {
  default = "172-17-177-11.sslip.io"
}

module "database" {
  depends_list = [
    kubernetes_namespace.keycloak
  ]
  source = "../../../k8s-zalando-postgres/modules/database"
  namespace = kubernetes_namespace.keycloak.metadata[0].name
  dbname = "keycloak"
  dbuser = "keycloak"
}

module "static_ingress_data" {
  source = "../../../k8s-static-data/ingress"
}

locals {
  ingress_keycloak_annotations =  {
     "ingress.kubernetes.io/proxy-body-size": "0",
     "nginx.ingresskubernetes.io/proxy-body-size": "0",
     "forecastle.stakater.com/appName":"keycloak",
     "forecastle.stakater.com/group": "utils",
     "forecastle.stakater.com/icon": "https://raw.githubusercontent.com/cncf/landscape/master/hosted_logos/keycloak.svg",
  }
}

locals {
  ingress_annotations  = {
      ingress = {
        annotations =  merge( module.static_ingress_data.default_annotations,local.ingress_keycloak_annotations)
      }
  }
}

resource "helm_release" "keycloak" {
  depends_on = [module.database.depend_on]
  name       = "keycloak"
  repository = "https://codecentric.github.io/helm-charts"
  chart      = "keycloak"
  namespace  = kubernetes_namespace.keycloak.metadata[0].name
  values = [
    "${templatefile("${path.module}/files/values.yml", {
      CLUSTER_DOMAIN = var.ingress_domain,
    })}",
    yamlencode(local.ingress_annotations)
  ]
  set {
    name  = "keycloak.existingSecret"
    value = kubernetes_secret.keycloak_admin_auth.metadata[0].name
  }

  version = "9.0.1"
}


variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    helm_release.keycloak,
    module.database.depend_on,
  ]
}

