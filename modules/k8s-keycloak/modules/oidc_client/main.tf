


variable "realm_name" {

}

data "keycloak_realm" "realm" {
  realm = var.realm_name
}

variable "service_redirect_uri" {
  type = list(string)
}

variable "keycloak_service_client_id" {

}
variable "keycloak_service_name" {

}

resource "keycloak_openid_client" "openid_client" {
  depends_on = [var.depends_list]

  realm_id  = "${data.keycloak_realm.realm.id}"
  client_id = "${var.keycloak_service_client_id}"

  name                  = var.keycloak_service_name
  enabled               = true
  standard_flow_enabled = true
  access_type           = "CONFIDENTIAL"
  valid_redirect_uris   = var.service_redirect_uri

}

output "client_secret" {
  value = keycloak_openid_client.openid_client.client_secret
}


variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    keycloak_openid_client.openid_client,
  ]
}

