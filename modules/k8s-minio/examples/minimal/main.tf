
module "prometheus_crds" {
  source            = "../../../k8s-monitoring/modules/crds"
  enable_crd_servicemonitors = true
  enable_crd_prometheusrules = false
  enable_crd_alertmanagers   = false
  enable_crd_podmonitors     = false
  enable_crd_prometheuses    = false
  enable_crd_thanosrulers    = false
}

module "minio" {
  depends_list      = [module.prometheus_crds.depend_on]
  source            = "../../modules/install"
  ingress_domain = "notexist.k8s.local"
}
