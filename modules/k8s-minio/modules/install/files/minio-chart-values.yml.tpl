---
# Original https://github.com/helm/charts/blob/master/stable/minio/values.yaml
replicas: 1
buckets: []
metrics:
  # Metrics can not be disabled yet: https://github.com/minio/minio/issues/7493
  serviceMonitor:
    enabled: true
    additionalLabels:
      prometheus: k3s-monitoring

environment:
  MINIO_PROMETHEUS_AUTH_TYPE: "public"

persistence:
  enabled: true
  storageClass: ${persistence_storageClass}
  VolumeName: ""
  accessMode: ReadWriteOnce
  size: ${persistence_size}

ingress:
  enabled: true
  annotations: {}
  path: /
  hosts:
    - minio.${ingress_hosts}
  paths:
    - /
  tls:
    - secretName: minio-cert
      hosts:
        - minio.${ingress_hosts}
