resource "kubernetes_namespace" "release" {
  depends_on = [var.depends_list]
  metadata {
    name = var.release_namespace
  }
}

resource "kubernetes_secret" "minio_admin_credentials" {
  metadata {
    name      = "minio-admin-credentials"
    namespace = kubernetes_namespace.release.metadata[0].name
  }

  data = {
    "accesskey" = var.minio_admin_access_key
    "secretkey" = var.minio_admin_secret_key
  }
  type = "Opaque"
}

module "static_ingress_data" {
  source = "../../../k8s-static-data/ingress"
}

locals {
  ingress_minio_annotations =  {
     "ingress.kubernetes.io/proxy-body-size": "0",
     "nginx.ingresskubernetes.io/proxy-body-size": "0",
     "forecastle.stakater.com/appName":"Minio",
     "forecastle.stakater.com/group": "utils",
     "forecastle.stakater.com/icon": "https://www.vectorlogo.zone/logos/minioio/minioio-ar21.svg",
  }
}

locals {
  ingress_annotations = {
      ingress = {
        annotations = merge( module.static_ingress_data.default_annotations,local.ingress_minio_annotations)
      }
  }
}

resource "helm_release" "release" {
  depends_on = [var.depends_list]
  name       = "minio"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = "minio"
  namespace  = kubernetes_namespace.release.metadata[0].name
  values = [
    "${templatefile("${path.module}/files/minio-chart-values.yml.tpl", {
      ingress_hosts            = var.ingress_domain,
      persistence_storageClass = var.minio_persistence_storageClass,
      persistence_size         = var.minio_persistence_size,
    })}",
    yamlencode(local.ingress_annotations)
  ]

  set {
    name  = "existingSecret"
    value = kubernetes_secret.minio_admin_credentials.metadata[0].name
  }

}
