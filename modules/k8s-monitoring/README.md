
```bash

# grafana admin pw
kubectl get secrets grafana -n monitoring-presentation -ojson | jq '.data["admin-password"]' -r | base64 -d
```
