
variable "operator_namespace" {
  default = "operators"
}


module "monitoring_operator" {
  depends_list = [var.depends_list]
  source       = "../operator_chart"

  namespace = var.operator_namespace
  values = [
    "${file("${path.module}/../operator_chart/files/prometheus-chart-disable-all.yaml")}",
    "${file("${path.module}/files/operator-only-values.yaml")}"
  ]
  prometheus_release_name = "prometheus-operator"
  skip_crds               = true
}



variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [module.monitoring_operator.depend_on]
}

