variable "prometheus_release_name" {
}


variable "namespace" {

}

variable "values" {
  type = list(string)

}

variable "chart_name" {
  default = "prometheus-operator"
}
variable "chart_version" {
  default = "8.14.0"
}

variable "skip_crds" {
  default = true
}


resource "helm_release" "operator" {
  name       = var.prometheus_release_name
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = var.chart_name
  version    = var.chart_version
  namespace  = var.namespace
  skip_crds  = var.skip_crds
  values     = var.values

}


variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [helm_release.operator]
}

