prometheus:
  prometheusSpec:
  # https://github.com/helm/charts/blob/master/stable/prometheus-operator/values.yaml
    alertingEndpoints:
      - name: ${ ALERTMANAGER_SERVICE_NAME }
        namespace: ${ ALERTMANAGER_SERVICE_NAMESPACE }
        port: web