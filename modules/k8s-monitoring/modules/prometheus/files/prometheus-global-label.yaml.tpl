commonLabels:
  prometheus: ${SELECTOR_VALUE}
alertmanager:
  alertmanagerSpec:
    nodeSelector:
      "beta.kubernetes.io/arch": "amd64"
prometheusOperator:
  admissionWebhooks:
    patch:
      nodeSelector:
        "beta.kubernetes.io/arch": "amd64"
  nodeSelector:
    "beta.kubernetes.io/arch": "amd64"
prometheus:
  prometheusSpec:
    nodeSelector:
      "beta.kubernetes.io/arch": "amd64"