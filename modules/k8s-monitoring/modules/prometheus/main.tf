
variable "ingress_domain" {
  default = "172-17-177-11.sslip.io"
}


resource "kubernetes_namespace" "presentation_monitoring" {
  depends_on = [var.depends_list]
  metadata {
    labels = {
      solution = "monitoring"
    }
    name = "monitoring-presentation"
  }
}

resource "kubernetes_namespace" "cluster_monitoring" {
  depends_on = [var.depends_list]
  metadata {
    labels = {
      solution = "monitoring"
    }
    name = "monitoring-cluster"
  }
}


variable "prometheus_cluster_moitoring_name" {
  default = "cluster-monitoring"
}



module "alerting" {
  source = "../operator_chart"

  namespace = kubernetes_namespace.presentation_monitoring.metadata[0].name
  values = [
    "${file("${path.module}/../operator_chart/files/prometheus-chart-disable-all.yaml")}",
    "${file("${path.module}/files/alertmanager-only-values.yaml")}"
  ]
  prometheus_release_name = "alertmanager"
}

data "kubernetes_service" "alertmanager" {
  depends_on = [module.alerting.depend_on]
  metadata {
    name      = "alertmanager-prometheus-op-alertmanager"
    namespace = kubernetes_namespace.presentation_monitoring.metadata[0].name
  }
}

module "presentation" {
  source                 = "./presentation"
  depends_list           = [module.alerting.depend_on]
  presentation_namespace = kubernetes_namespace.presentation_monitoring.metadata[0].name
  alertmanager_endpoint  = "http://${data.kubernetes_service.alertmanager.metadata[0].name}.${data.kubernetes_service.alertmanager.metadata[0].namespace}.svc:9093"
  ingress_domain         = var.ingress_domain
}


module "cluster_monitoring" {
  depends_list = [module.alerting.depend_on]
  source       = "../operator_chart"

  namespace = kubernetes_namespace.cluster_monitoring.metadata[0].name
  values = [
    "${file("${path.module}/../operator_chart/files/prometheus-chart-disable-all.yaml")}",
    "${templatefile("${path.module}/files/cluster-monitor-values.yaml", {
      SELECTOR_VALUE = var.prometheus_cluster_moitoring_name
    })}",
    "${templatefile("${path.module}/files/prometheus-alertmanager.yaml.tpl", {
      ALERTMANAGER_SERVICE_NAME      = data.kubernetes_service.alertmanager.metadata[0].name,
      ALERTMANAGER_SERVICE_NAMESPACE = kubernetes_namespace.presentation_monitoring.metadata[0].name
    })}",
    "${templatefile("${path.module}/files/prometheus-global-label.yaml.tpl", {
      SELECTOR_VALUE = var.prometheus_cluster_moitoring_name
    })}",
  ]
  prometheus_release_name = var.prometheus_cluster_moitoring_name
}



resource "kubernetes_secret" "datasource_thanos" {
  depends_on = [module.cluster_monitoring.depend_on]
  metadata {
    name      = "grafana-datasource"
    namespace = kubernetes_namespace.cluster_monitoring.metadata[0].name
    labels = {
      grafana_datasource = "1"
    }
  }

  data = {
    "datasource.yaml" = "${templatefile("${path.module}/files/grafana-prometheus-datasource.yml.tpl", {})}"
  }

}

output "namespace" {
  value = kubernetes_namespace.cluster_monitoring.metadata[0].name
}

variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [module.alerting.depend_on, module.presentation.depend_on, module.cluster_monitoring.depend_on, kubernetes_secret.datasource_thanos]
}

