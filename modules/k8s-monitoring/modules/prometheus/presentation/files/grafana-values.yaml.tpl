# https://github.com/helm/charts/blob/master/stable/grafana/values.yaml
ingress:
    enabled: true
    annotations: {}
    path: /
    hosts:
        - grafana.${ CLUSTER_DOMAIN }
    tls:
        - secretName: grafana-cert
          hosts:
              - grafana.${ CLUSTER_DOMAIN }
sidecar:
    dashboards:
        enabled: true
        searchNamespace: "ALL"
    datasources:
        enabled: true
        searchNamespace: "ALL"

plugins:
    []
    #- grafana-kubernetes-app
