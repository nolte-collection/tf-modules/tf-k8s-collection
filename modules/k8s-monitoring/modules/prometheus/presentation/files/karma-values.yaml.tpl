# https://github.com/helm/charts/blob/master/stable/karma/values.yaml

image:
  repository: lmierzwa/karma
  tag: v0.60

env:
- name: ALERTMANAGER_URI
  value: ${ALERTMANAGER_URL}

nodeSelector:
  "beta.kubernetes.io/arch": "amd64"

livenessProbe:
  delay: 45
  period: 5
  path: /health

ingress:
  enabled: true
  annotations: {}
  path: /
  hosts:
    - karma.${ CLUSTER_DOMAIN }
  tls:
    - secretName: karma-cert
      hosts:
        - karma.${ CLUSTER_DOMAIN }
