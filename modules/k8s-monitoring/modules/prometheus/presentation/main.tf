variable "presentation_namespace" {

}

variable "alertmanager_endpoint" {

}

module "static_ingress_data" {
  source = "../../../../k8s-static-data/ingress"
}

locals {
  ingress_commons_annotations =  {
     #"forecastle.stakater.com/icon": "https://raw.githubusercontent.com/goharbor/website/master/static/img/logos/harbor-icon-color.png",
     "forecastle.stakater.com/group": "monitoring",
     "ingress.kubernetes.io/proxy-body-size": "0",
     "nginx.ingresskubernetes.io/proxy-body-size": "0",
  }
  ingress_defaults_annotations = merge( module.static_ingress_data.default_annotations,local.ingress_commons_annotations)
  ingress_grafana_annotations =  {
    "forecastle.stakater.com/appName":"Grafana",
    "forecastle.stakater.com/icon": "https://www.vectorlogo.zone/logos/grafana/grafana-ar21.svg",
  }
  ingress_karma_annotations =  {
    "forecastle.stakater.com/appName":"Karma",
  }
}
locals {
  ingress_karma_full_annotations  = {
      ingress = {
        annotations = merge( local.ingress_karma_annotations,local.ingress_defaults_annotations)
      }
  }
  ingress_grafana_full_annotations  = {
      ingress = {
        annotations = merge( local.ingress_grafana_annotations,local.ingress_defaults_annotations)
      }
  }
}

variable "ingress_domain" {
  default = "172-17-177-11.sslip.io"
}

resource "helm_release" "karma" {
  name       = "karma"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = "karma"
  version    = "1.5.1"
  namespace  = var.presentation_namespace
  timeout = 600
  values = ["${templatefile("${path.module}/files/karma-values.yaml.tpl", {
    ALERTMANAGER_URL = var.alertmanager_endpoint
    CLUSTER_DOMAIN   = var.ingress_domain,
  })}",
  yamlencode(local.ingress_karma_full_annotations)
 ]

}

//
resource "helm_release" "grafana" {
  name       = "grafana"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = "grafana"
  #version    = "5.0.9"
  namespace = var.presentation_namespace
  timeout = 600
  values = [
    "${templatefile("${path.module}/files/grafana-values.yaml.tpl", { CLUSTER_DOMAIN = var.ingress_domain, })}",
    yamlencode(local.ingress_grafana_full_annotations)
  ]
}

//

variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [helm_release.karma]
}
