
variable "namespace" {
  default = "operators"
}

variable "skip_crds" {
  default = false
}

resource "helm_release" "release" {
  name       = "reloader"
  repository = "https://stakater.github.io/stakater-charts"
  chart      = "reloader"
  namespace  = var.namespace
  create_namespace = true
  skip_crds = var.skip_crds

}
