


output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    helm_release.release,
  ]
}
