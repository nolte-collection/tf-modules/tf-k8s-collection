output "default_annotations" {
  value = {
    "cert-manager.io/cluster-issuer": "cluster-issuer",
    "ingress.kubernetes.io/ssl-redirect": "true",
    "nginx.ingress.kubernetes.io/ssl-redirect": "true",
    "forecastle.stakater.com/expose": "true",
    "kubernetes.io/ingress.class": "nginx",
  }
}
