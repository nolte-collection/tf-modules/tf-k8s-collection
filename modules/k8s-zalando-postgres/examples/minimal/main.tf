
module "install" {
  source = "../../modules/install"
}

# give postgress operator Time to Work
resource "time_sleep" "wait_seconds" {
  depends_on = [module.install.depend_on]

  create_duration = "2s"
}

module "database" {
  depends_list = [
    time_sleep.wait_seconds,
    module.install.depend_on
  ]
  source = "../../modules/database"
  namespace = "default"
  dbname = "testapp"
  dbuser = "testapp"
}
