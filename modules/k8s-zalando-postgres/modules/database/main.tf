variable "namespace" {

}

variable "teamId" {
  default = "baseline"
}
variable "dbname" {
}

variable "dbuser" {
}
variable "volume_size" {
  default ="1Gi"
}
variable "numberOfInstances" {
  default ="1"
}
resource "kubectl_manifest" "database" {
  depends_on = [var.depends_list]
  yaml_body  = <<YAML
apiVersion: "acid.zalan.do/v1"
kind: postgresql
metadata:
  name: ${var.teamId}-${var.dbname}
  namespace: ${var.namespace}
spec:
  teamId: "${var.teamId}"
  volume:
    size: "${var.volume_size}"
  numberOfInstances: ${var.numberOfInstances}
  users:
    ${var.dbuser}: # database owner
      - superuser
      - createdb
    ${var.dbuser}_user: [] # role for application foo
  databases:
    ${var.dbname}: ${var.dbuser} # dbname: owner
  preparedDatabases:
    ${var.dbname}: {}
  postgresql:
    version: "12"
YAML
}


variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    kubectl_manifest.database,
  ]
}

