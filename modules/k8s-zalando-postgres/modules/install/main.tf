data "http" "configmap" {
  url = "https://raw.githubusercontent.com/zalando/postgres-operator/v1.5.0/manifests/configmap.yaml"
}

resource "kubectl_manifest" "configmap" {
  yaml_body = data.http.configmap.body
}

data "http" "operator_service_account_rbac" {
  url = "https://raw.githubusercontent.com/zalando/postgres-operator/v1.5.0/manifests/operator-service-account-rbac.yaml"
}
#
resource "kubectl_manifest" "operator_service_account_rbac" {
  depends_on = [kubectl_manifest.configmap]
  yaml_body  = data.http.operator_service_account_rbac.body
}

#
data "http" "postgres_operator" {
  url = "https://raw.githubusercontent.com/zalando/postgres-operator/v1.5.0/manifests/postgres-operator.yaml"
}
#
resource "kubectl_manifest" "postgres_operator" {
  depends_on = [kubectl_manifest.operator_service_account_rbac]
  yaml_body  = data.http.postgres_operator.body
}
#
data "http" "api_service" {
  url = "https://raw.githubusercontent.com/zalando/postgres-operator/v1.5.0/manifests/api-service.yaml"
}
#
resource "kubectl_manifest" "api_service" {
  depends_on = [kubectl_manifest.postgres_operator]
  yaml_body  = data.http.api_service.body
}
