

output "release_namespace" {
  value = kubectl_manifest.postgres_operator.namespace
}


output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    kubectl_manifest.configmap,
    kubectl_manifest.operator_service_account_rbac,
    kubectl_manifest.postgres_operator,
    kubectl_manifest.api_service,
  ]
}
